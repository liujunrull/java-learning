## 计算机和编程语言

### 程序的执行

解释：借助一个程序，那个程序能试图理解你的程序，然后按照你的要求执行

编译：借助一个程序，就像一个翻译，把你的程序翻译成计算机真正能懂的语言-机器语言-写的程序，然后，这个机器语言写的程序就能直接执行了

![image-20230913211810646](https://gitee.com/liujunrull/image-blob/raw/master/202309232019477.png)

![image-20230913220306594](https://gitee.com/liujunrull/image-blob/raw/master/202309132203669.png)

#### 数据类型

整数

```c
int
printf("%d",...)
scanf("%d",...)
```

浮点数

```c
double
printf("%f",...)
scanf("%lf",...)
```

![image-20230923202425383](https://gitee.com/liujunrull/image-blob/raw/master/202309232024705.png)

### 指针

```c
const int *p = &i;
*p = 26;//Error,(*p)是const
i= 26;//OK
p = &j;//ok
```

const int *p表示不能通过这个指针去修改这个变量，p可以变，i可以变，但不能通过p去改变i的值