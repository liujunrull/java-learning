## 1.概念

#### 反向代理

![image-20230721134504381](https://gitee.com/liujunrull/image-blob/raw/master/202307211345645.png)

![image-20230721134544753](https://gitee.com/liujunrull/image-blob/raw/master/202307211345805.png)

反向代理隐藏了真实的服务端，当我们请求 [http://www.baidu.com](https://link.zhihu.com/?target=http%3A//www.baidu.com) 的时候，就像拨打 QQ 客服热线一样，背后可能有成千上万台服务器为我们服务，但具体是哪一台，你不知道，也不需要知道，你只需要知道反向代理服务器是谁就好了，[http://www.baidu.com](https://link.zhihu.com/?target=http%3A//www.baidu.com) 就是我们的反向代理服务器，反向代理服务器会帮我们把请求转发到真实的服务器那里去。

正向代理代理的对象是客户端，反向代理代理的对象是服务端。

#### 边车模式

Service Mesh 将底层那些难以控制的网络通讯统一管理，诸如：流量管控，丢包重试，访问控制等。而上层的应用层协议只需关心业务逻辑即可。Service Mesh 是一个用于处理服务间通信的基础设施层，它负责为构建复杂的云原生应用传递可靠的网络请求。

![image-20230721135122434](https://gitee.com/liujunrull/image-blob/raw/master/202307211351525.png)

````xml
<dependencies>
    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-zuul</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-eureka</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-netflix-sidecar</artifactId>
    </dependency>
  </dependencies>

作者：Iron_man
链接：https://juejin.cn/post/6844904161864073230
来源：稀土掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
````

````java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.sidecar.EnableSidecar;

@SpringBootApplication
@EnableSidecar
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(SidecarApplication.class, args);
  }
}

作者：Iron_man
链接：https://juejin.cn/post/6844904161864073230
来源：稀土掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
````



````yaml
server:
  port: 8070
spring:
  application:
    name: microservice-sidecar-node-service
eureka:
  client:
    service-url:
      defaultZone:http://localhost:8761/eureka/
  instance:
    prefer-ip-address: true
sidecar:
  # 你的PHP微服务的端口
  port: 8060                                    
  # PHP微服务的健康检查URL
  health-uri:http://localhost:8060/health.json# 

作者：Iron_man
链接：https://juejin.cn/post/6844904161864073230
来源：稀土掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
````

