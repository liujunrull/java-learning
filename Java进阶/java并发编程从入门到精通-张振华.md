####  线程的中断

1) Thread.stop()

2) Thread.interrupt()

```java
public static boolean interrupt():测试当前线程是否已经中断，线程的中断状态由该方法清除
public boolean isinterrupt():测试线程是否已经中断
public void interrupt():中断线程，但是没有返回结果
```

被中断的线程一定要对isInterrupted状态进行处理，否则如果代码是死循环的情况下，线程将永远不会结束。

#### 线程常用方法

```
static void sleep(long millis):释放CPU的权，不释放锁
final void wait():释放CPU的执行权，释放锁，wait()必须在synchronized block中调用
```

#### 当前线程副本：ThreadLocal

```java
void set(T value):设置当前线程的线程局部变量的值
public T get():该方法返回当前线程所对应的线程局部变量
public void remove():将当前线程局部变量的值删除，目的是减少内存的占用
protected T initialValue():返回该线程局部变量的初始值
```

#### Fork/Join框架

使用工作窃取算法，将一个大任务分解为若干互不依赖的子任务，为了减少线程间的竞争，于是将这些子任务分别放到不同的队列里，并为每个队列创建一个单独的线程来执行队列里的任务，线程和队列一一对应。线程处理快的会去其他线程的队列里窃取一个任务来执行，通常会使用双端队列，被窃取任务线程永远从双端队列的头部拿任务执行，而窃取任务的线程永远从双端队列的尾部拿任务执行。

优点是重复利用线程进行并行计算，并减少了线程间的竞争，其缺点是在某些情况下还是存在竞争，比如双端队列里只有一个任务时，并且消耗了更多的系统资源，比如创建多个线程和多个双端队列。

#### Nginx线程池

大并发的数量级别的处理方式：

1）最常见的，运维简单的A10、F5的硬件负载均衡器，直接进行请求转发。这时候基本上不需要Nginx，直接到后台Server了。当然加Nginx更好，可以再扩大N倍

2）通过域名解析，建立很多子域名，不同的业务模块由域名分发到不同的Nginx服务器上面去，每个Nginx再都配置上内容分发网络（CDN），实行静态分离，尽量给Nginx服务器减少负担。

#### 数据库连接池

当我们创建了一个Connection对象，他在内部都执行了什么：

1）DriverManager检查并注册驱动程序

2）com.mysql.jdbc.Driver就是我们注册了的驱动程序，他会在驱动程序类中调用connect(url...)方法

3）com.mysql.jdbc.Driver的connect方法根据我们请求的ConnUrl，创建一个Socket对象，连接到IP为your.database.domain，默认端口为3306的数据库

4）创建的socket连接将被用来查询我们指定的数据库，并最终让程序返回得到一个结果。