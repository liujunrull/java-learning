# docker安装

### docker理念

Docker是基于Go语言实现的云开源项目。

Docker的主要目标是“Build，Ship and Run Any App,Anywhere”，也就是通过对应用组件的封装、分发、部署、运行等生命周期的管理，使用户的APP（可以是一个WEB应用或数据库应用等等）及其运行环境能够做到“一次镜像，处处运行”。

Linux容器技术的出现就解决了这样一个问题，而 Docker 就是在它的基础上发展过来的。将应用打成镜像，通过镜像成为运行在Docker容器上面的实例，而 Docker容器在任何操作系统上都是一致的，这就实现了跨平台、跨服务器。只需要一次配置好环境，换到别的机子上就可以一键部署好，大大简化了操作。

 一句话，  解决了运行环境和配置问题的软件容器， 方便做持续集成并有助于整体发布的容器虚拟化技术。

### docker官网

·     官网

·     docker官网：http://www.docker.com

·     仓库

·     Docker Hub官网: https://hub.docker.com/

### Docker的基本组成

#### 镜像(image)

Docker 镜像（Image）就是一个**只读**的模板。镜像可以用来创建 Docker 容器，一个镜像可以创建很多容器。

它也相当于是一个root文件系统。比如官方镜像 centos:7 就包含了完整的一套 centos:7 最小系统的 root 文件系统。

相当于容器的“源代码”，docker镜像文件类似于Java的类模板，而docker容器实例类似于java中new出来的实例对象。

#### 容器(container)

1. 从面向对象角度

Docker 利用容器（Container）独立运行的一个或一组应用，应用程序或服务运行在容器里面，容器就类似于一个虚拟化的运行环境，容器是用镜像创建的运行实例。就像是Java中的类和实例对象一样，镜像是静态的定义，容器是镜像运行时的实体。容器为镜像提供了一个标准的和隔离的运行环境，它可以被启动、开始、停止、删除。每个容器都是相互隔离的、保证安全的平台

 

2.  从镜像容器角度

可以把容器看做是一个简易版的Linux环境（包括root用户权限、进程空间、用户空间和网络空间等）和运行在其中的应用程序。

![](https://gitee.com/liujunrull/image-blob/raw/master/202301181435459.png)

#### 仓库(repository)

仓库（Repository）是集中存放镜像文件的场所。

 

类似于

Maven仓库，存放各种jar包的地方；

github仓库，存放各种git项目的地方；

Docker公司提供的官方registry被称为Docker Hub，存放各种镜像模板的地方。

 

仓库分为公开仓库（Public）和私有仓库（Private）两种形式。

最大的公开仓库是 Docker Hub(https://hub.docker.com/)，

存放了数量庞大的镜像供用户下载。国内的公开仓库包括阿里云 、网易云等



#### **Docker平台架构**

![](https://gitee.com/liujunrull/image-blob/raw/master/202301181514102.png)

####   CentOS7安装Docker

   https://docs.docker.com/engine/install/centos/

#####  安装步骤

- 确定你是CentOS7及以上版本


```
cat /etc/redhat-release
```

- 卸载旧版本


 https://docs.docker.com/engine/install/centos/

-  yum安装gcc相关


CentOS7能上外网

```
yum -y install gcc
yum -y install gcc-c++
```

- 安装需要的软件包

```
yum install -y yum-utils
```

- 设置stable镜像仓库

  ```
  yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
  ```

- 更新yum软件包索引

```
yum makecache fast
```

- 安装DOCKER CE

```
yum -y install docker-ce docker-ce-cli containerd.io
```

- 启动docker

```
systemctl start docker
```

- 测试

```
docker version
docker run hello-world
```

- 卸载

```
systemctl stop docker 
yum remove docker-ce docker-ce-cli containerd.io
rm -rf /var/lib/docker
rm -rf /var/lib/containerd
```



·     启动docker

·     systemctl start docker

·     测试

·     docker version

#### **阿里云镜像加速**

·     https://promotion.aliyun.com/ntms/act/kubernetes.html

·     注册一个属于自己的阿里云账户(可复用淘宝账号)

·     获得加速器地址连接

·     登陆阿里云开发者平台

·     点击控制台

·     选择容器镜像服务

·     获取加速器地址

·     粘贴脚本直接执行

| mkdir -p  /etc/docker                                        |
| ------------------------------------------------------------ |
| tee  /etc/docker/daemon.json <<-'EOF'  {     "registry-mirrors": ["https://aa25jngu.mirror.aliyuncs.com"]  }  EOF |

·   重启服务器

```
systemctl daemon-reload
systemctl restart docker
```



##### 为什么Docker会比VM虚拟机快

(1)docker有着比虚拟机更少的抽象层

  由于docker不需要Hypervisor(虚拟机)实现硬件资源虚拟化,运行在docker容器上的程序直接使用的都是实际物理机的硬件资源。因此在CPU、内存利用率上docker将会在效率上有明显优势。

(2)docker利用的是宿主机的内核,而不需要加载操作系统OS内核

  当新建一个容器时,docker不需要和虚拟机一样重新加载一个操作系统内核。进而避免引寻、加载操作系统内核返回等比较费时费资源的过程,当新建一个虚拟机时,虚拟机软件需要加载OS,返回新建过程是分钟级别的。而docker由于直接利用宿主机的操作系统,则省略了返回过程,因此新建一个docker容器只需要几秒钟。

![](https://gitee.com/liujunrull/image-blob/raw/master/202301181556159.png)

### Docker常用命令

####   **帮助启动类命令**

·     启动docker： systemctl start docker

·     停止docker： systemctl stop docker

·     重启docker： systemctl restart docker

·     查看docker状态： systemctl status docker

·     开机启动： systemctl enable docker

·     查看docker概要信息： docker info

·     查看docker总体帮助文档： docker --help

·     查看docker命令帮助文档： docker 具体命令 --help

·     查看docker命令帮助文档： docker 具体命令 --help

#### **镜像命令**

```
  docker images
```

![](https://gitee.com/liujunrull/image-blob/raw/master/202301181558672.png)

REPOSITORY：表示镜像的仓库源

TAG：镜像的标签版本号

IMAGE ID：镜像ID

CREATED：镜像创建时间

SIZE：镜像大小

同一仓库源可以有多个 TAG版本，代表这个仓库源的不同个版本，我们使用 REPOSITORY:TAG 来定义不同的镜像。

如果你不指定一个镜像的版本标签，例如你只使用 ubuntu，docker 将默认使用 ubuntu:latest 镜像

·     OPTIONS说明：

·     -a :列出本地所有的镜像（含历史映像层）

·     -q :只显示镜像ID。

·     docker search 某个XXX镜像名字

·     网站

·     https://hub.docker.com

·     命令

```
docker search [OPTIONS] 镜像名字
```

·     OPTIONS说明：

·     --limit : 只列出N个镜像，默认25个

·     docker search --limit 5 redis

```
 docker pull 某个XXX镜像名字
```

·     下载镜像

·     docker pull 镜像名字[:TAG]

·     docker pull 镜像名字

·     没有TAG就是最新版

·     等价于

·     docker pull 镜像名字:latest

·     docker pull ubuntu

```
docker system df 查看镜像/容器/数据卷所占的空间
```

```
docker rmi 某个XXX镜像名字ID
```

·     删除镜像

·     删除单个

```
  docker rmi -f 镜像ID
```

·     删除多个

```
   docker rmi -f 镜像名1:TAG 镜像名2:TAG
```

·     删除全部

```
  docker rmi -f $(docker images -qa)
```

**  面试题：谈谈docker虚悬镜像是什么？***

·     是什么

·     仓库名、标签都是<none>的镜像，俗称虚悬镜像dangling image

·     长什么样

![](https://gitee.com/liujunrull/image-blob/raw/master/202301181602789.png)

#### 容器命令

·     docker pull centos

·     docker pull ubuntu

- 新建+启动容器


```
 docker run [OPTIONS] **IMAGE** [COMMAND] [ARG...]
```

·     OPTIONS说明

 OPTIONS说明（常用）：有些是一个减号，有些是两个减号

 

--name="容器新名字"    为容器指定一个名称；

-d: 后台运行容器并返回容器ID，也即启动守护式容器(后台运行)；

 

-i：以交互模式运行容器，通常与 -t 同时使用；

-t：为容器重新分配一个伪输入终端，通常与 -i 同时使用；

也即启动交互式容器(前台有伪终端，等待交互)；

 

-P: 随机端口映射，大写P

-p: 指定端口映射，小写p

\#使用镜像centos:latest以交互模式启动一个容器,在容器内执行/bin/bash命令。

```
docker run -it centos /bin/bash 
```

参数说明：

-i: 交互式操作。

-t: 终端。

centos : centos 镜像。

/bin/bash：放在镜像名后的是命令，这里我们希望有个交互式 Shell，因此用的是 /bin/bash。

要退出终端，直接输入 exit:

- 列出当前所有正在运行的容器


```
 docker ps [OPTIONS]
```

·     OPTIONS说明

OPTIONS说明（常用）：

 

-a :列出当前所有正在运行的容器+历史上运行过的

-l :显示最近创建的容器。

-n：显示最近n个创建的容器。

-q :静默模式，只显示容器编号。

- 退出容器


·     两种退出方式

exit

·     run进去容器，exit退出，容器停止

  ctrl+p+q

·     run进去容器，ctrl+p+q退出，容器不停止

- 启动已停止运行的容器


```
 docker start 容器ID或者容器名
```

-   重启容器


```
 docker restart 容器ID或者容器名
```

-   停止容器


```
 docker stop 容器ID或者容器名
```

-  强制停止容器


```
 docker kill 容器ID或容器名
```

- 删除已停止的容器 


```
docker rm 容器ID
```

-   一次性删除多个容器实例


```
docker rm -f $(docker ps -a -q)

docker ps -a -q | xargs docker rm
```

- 启动守护式容器(后台服务器)

在大部分的场景下，我们希望 docker 的服务是在后台运行的，我们可以过 -d 指定容器的后台运行模式。

```
docker run -d 容器名
```

redis演示

```
前台交互式启动	docker run -it redis:6.0.8
后台守护式启动 docker run -d redis:6.0.8
```

- 查看容器日志

```
docker logs 容器ID
```

- 查看容器内运行的进程

```
docker top 容器ID
```

- 查看容器内部细节

  ```
  docker inspect 容器ID
  ```

- 进入正在运行的容器并以命令行交互

```
docker exec -it 容器ID bashShell
重新进入docker attach 容器ID
```

上述两个区别：

attach 直接进入容器启动命令的终端，不会启动新的进程用exit退出，会导致容器的停止。

exec 是在容器中打开新的终端，并且可以启动新的进程用exit退出，不会导致容器的停止。

推荐大家使用 docker exec 命令，因为退出容器终端，不会导致容器的停止

- 从容器内拷贝文件到主机上

```
容器→主机  docker cp  容器ID:容器内路径 目的主机路径
```

- 导入和导出容器

export 导出容器的内容留作为一个tar归档文件[对应import命令]

import 从tar包中的内容创建一个新的文件系统再导入为镜像[对应export]

Docker镜像

是一种轻量级、可执行的独立软件包，它包含运行某个软件所需的所有内容，我们把应用程序和配置依赖打包好形成一个可交付的运行环境(包括代码、运行时需要的库、环境变量和配置文件等)，这个打包好的运行环境就是image镜像文件。

只有通过这个镜像文件才能生成Docker容器实例(类似Java中new出来一个对象)。

**UnionFS（联合文件系统）**

拟文件系统下(unite several directories into a single virtual filesystem)。Union 文件系统是 Docker 镜像的基础。镜像可以通过分层来进行继承，基于基础镜像（没有父镜像），可以制作各种具体的应用镜像。

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142204669.png)

特性：一次同时加载多个文件系统，但从外面看起来，只能看到一个文件系统，联合加载会把各层文件系统叠加起来，这样最终的文件系统会包含所有底层的文件和目录

**Docker镜像加载原理：**

  docker的镜像实际上由一层一层的文件系统组成，这种层级的文件系统UnionFS。

bootfs(boot file system)主要包含bootloader和kernel, bootloader主要是引导加载kernel, Linux刚启动时会加载bootfs文件系统，在Docker镜像的最底层是引导文件系统bootfs。这一层与我们典型的Linux/Unix系统是一样的，包含boot加载器和内核。当boot加载完成之后整个内核就都在内存中了，此时内存的使用权已由bootfs转交给内核，此时系统也会卸载bootfs。

rootfs (root file system) ，在bootfs之上。包含的就是典型 Linux 系统中的 /dev, /proc, /bin, /etc 等标准目录和文件。rootfs就是各种不同的操作系统发行版，比如Ubuntu，Centos等等。

。 ![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/1C0CCAA4-863C-42D2-AA4A-926320AFB589.png)

 平时我们安装进虚拟机的CentOS都是好几个G，为什么docker这里才200M？？

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142205991.png)

对于一个精简的OS，rootfs可以很小，只需要包括最基本的命令、工具和程序库就可以了，因为底层直接用Host的kernel，自己只需要提供 rootfs 就行了。由此可见对于不同的linux发行版, bootfs基本是一致的, rootfs会有差别, 因此不同的发行版可以公用bootfs。

**为什么 Docker 镜像要采用这种分层结构呢**

镜像分层最大的一个好处就是共享资源，方便复制迁移，就是为了复用。

比如说有多个镜像都从相同的 base 镜像构建而来，那么 Docker Host 只需在磁盘上保存一份 base 镜像；

同时内存中也只需加载一份 base 镜像，就可以为所有容器服务了。而且镜像的每一层都可以被共享。



**Docker镜像层都是只读的，容器层是可写的。当容器启动时，一个新的可写层被加载到镜像的顶部。这一层通常被称作“容器层”，“容器层”之下的都叫“镜像层”。**

### Docker镜像commit操作案例

```
docker commit提交容器副本使之成为一个新的镜像
docker commit -m="提交的描述信息" -a="作者" 容器ID 要创建的目标镜像名:[标签名]
```

#### 案例演示ubuntu安装vim

- 从Hub上下载ubuntu镜像到本地并成功运行

- 原始的默认Ubuntu镜像是不带着vim命令的

- 外网连通的情况下，安装vim

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142208840.png)

docker容器内执行上述两条命令：

```
apt-get update

apt-get -y install vim
```

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142208843.png)

![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/96021AF7-20D6-4AC0-BF90-9D5BA3A57866.png)

- 安装完成后，commit我们自己的新镜像

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142209872.png)

![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/DF68B4FD-883B-42E7-AD66-5676E1A7E38E.png)

- 启动我们的新镜像并和原来的对比

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142209770.png)

1 官网是默认下载的Ubuntu没有vim命令

2我们自己commit构建的镜像，新增加了vim功能，可以成功使用。

### 总结

Docker中的镜像分层，支持通过扩展现有镜像，创建新的镜像。类似Java继承于一个Base基础类，自己再按需扩展。

新镜像是从 base 镜像一层一层叠加生成的。每安装一个软件，就在现有镜像的基础上增加一层

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142210301.png)

## 本地镜像发布到阿里云

### 本地镜像发布到阿里云流程

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142211101.png)

### 镜像的生成方法

基于当前容器创建一个新的镜像，新功能增强

```
docker commit [OPTIONS] 容器ID [REPOSITORY[:TAG]]
```

OPTIONS说明：

-a :提交的镜像作者；

-m :提交时的说明文字；

本次案例centos+ubuntu两个，当堂讲解一个，家庭作业一个，请大家务必动手，亲自实操。

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142212604.png)

![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/369F113D-A589-4038-9E03-04C6B2C847BD.png)

### 将本地镜像推送到阿里云

- 本地镜像素材原型

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142213543.png)







![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/A776EB99-601C-49F7-BD4B-7AAC11A35386.png)

- 阿里云开发者平台

https://promotion.aliyun.com/ntms/act/kubernetes.html

- 创建仓库镜像

选择控制台，进入容器镜像服务

选择个人实例

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142214818.png)

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142214318.png)

![image-20230514221444459](https://gitee.com/liujunrull/image-blob/raw/master/202305142214561.png)

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142214802.png)

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142215375.png)

![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/FDE7508E-52C7-4F9C-A7C2-57D6B0E80582.png)

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142215749.png)

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142216404.png)

- 将镜像推送到阿里云

  将镜像推送到阿里云registry

  docker login --username=zzyybuy registry.cn-hangzhou.aliyuncs.com
  docker tag cea1bb40441c registry.cn-hangzhou.aliyuncs.com/atguiguwh/myubuntu:1.1
  docker push registry.cn-hangzhou.aliyuncs.com/atguiguwh/myubuntu:1.1
  上面命令是阳哥自己本地的，你自己酌情处理，不要粘贴我的。


### 将阿里云上的镜像下载到本地

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142218620.png)



```
docker pull registry.cn-hangzhou.aliyuncs.com/atguiguwh/myubuntu:1.1
```

## 本地镜像发布到私有库

### 是什么 Docker Registry

1 官方Docker Hub地址：https://hub.docker.com/，中国大陆访问太慢了且准备被阿里云取代的趋势，不太主流。



2 Dockerhub、阿里云这样的公共镜像仓库可能不太方便，涉及机密的公司不可能提供镜像给公网，所以需要创建一个本地私人仓库供给团队使用，基于公司内部项目构建镜像。



  Docker Registry是官方提供的工具，可以用于构建私有镜像仓库

### 将本地镜像推送到私有库

- 下载镜像Docker Registry

docker pull registry

![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/559DC469-744C-470B-86A3-9B1DBB050F7B.png)

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142220200.png)

- 运行私有库Registry，相当于本地有个私有**Docker hub **

```
docker run -d -p 5000:5000 -v /zzyyuse/myregistry/:/tmp/registry --privileged=true registry
```

默认情况，仓库被创建在容器的/var/lib/registry目录下，建议自行用容器卷映射，方便于宿主机联调

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142221926.png)

- curl验证私服库上有什么镜像

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142222484.png)

- 将新镜像zzyyubuntu:1.2修改符合私服规范的Tag

  按照公式： 

  ```
  docker  tag  镜像:Tag  Host:Port/Repository:Tag
  ```

  自己host主机IP地址，填写同学你们自己的，不要粘贴错误，O(∩_∩)O

  使用命令 docker tag 将zzyyubuntu:1.2 这个镜像修改为192.168.111.162:5000/zzyyubuntu:1.2

  ```
  docker tag zzyyubuntu:1.2 192.168.111.162:5000/zzyyubuntu:1.2
  ```

  ![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142222388.png)

- 修改配置文件使之支持http

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142223244.png)



别无脑照着复制，registry-mirrors 配置的是国内阿里提供的镜像加速地址，不用加速的话访问官网的会很慢。 *2个配置中间有个逗号 ','别漏了*，这个配置是json格式的。 *2个配置中间有个逗号 ','别漏了*，这个配置是json格式的。 *2个配置中间有个逗号 ','别漏了*，这个配置是json格式的。

vim命令新增如下红色内容：vim /etc/docker/daemon.json

{  "registry-mirrors": ["https://aa25jngu.mirror.aliyuncs.com"],  "insecure-registries": ["192.168.111.162:5000"] }

上述理由：docker默认不允许http方式推送镜像，通过配置选项来取消这个限制。====> 修改完后如果不生效，建议重启docker

- push推送到私服库

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142223471.png)

- curl验证私服库上有什么镜像2

  ```
  curl -XGET http://192.168.111.162:5000/v2/_catalog
  ```

  ![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142224135.png)

- pull到本地并运行

```
docker pull 192.168.111.162:5000/zzyyubuntu:1.2
```

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142224878.png)

```
docker run -it 镜像ID /bin/bash
```

![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/25C7841F-0406-47EB-82F3-7E82B6F6B449.png)

## Docker容器数据卷

Docker挂载主机目录访问如果出现cannot open directory .: Permission denied

解决办法：在挂载目录后多加一个--privileged=true参数即可

如果是CentOS7安全模块会比之前系统版本加强，不安全的会先禁止，所以目录挂载的情况被默认为不安全的行为，

在SELinux里面挂载目录被禁止掉了额，如果要开启，我们一般使用--privileged=true命令，扩大容器的权限解决挂载目录没有权限的问题，也即

使用该参数，container内的root拥有真正的root权限，否则，container内的root只是外部的一个普通用户权限。

还记得蓝色框框中的内容吗？

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142225036.png)

一句话：有点类似我们Redis里面的rdb和aof文件

将docker容器内的数据保存进宿主机的磁盘中

运行一个带有容器卷存储功能的容器实例

```
 docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录      镜像名
```

**作用**

\* 将运用与运行的环境打包镜像，run后形成容器实例运行 ，但是我们对数据的要求希望是持久化的

Docker容器产生的数据，如果不备份，那么当容器实例删除后，容器内的数据自然也就没有了。

为了能保存数据在docker中我们使用卷。

特点：

1：数据卷可在容器之间共享或重用数据

2：卷中的更改可以直接实时生效，爽

3：数据卷中的更改不会包含在镜像的更新中

4：数据卷的生命周期一直持续到没有容器使用它为止

### 数据卷案例

#### 宿主vs容器之间映射添加容器卷

直接命令添加

```
 docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录      镜像名
```

查看数据卷是否挂载成功

```
docker inspect 容器ID
```

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142227477.png)

容器和宿主机之间数据共享

1 docker修改，主机同步获得 

2 主机修改，docker同步获得

3 docker容器stop，主机修改，docker容器重启看数据是否同步。

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142227991.png)

#### 读写规则映射添加说明

**读写(默认)**

![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/52850F77-3DEA-4C9D-A314-12C8F25E859B.png)

 rw = read + write

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142228092.png)

```
 docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录:rw      镜像名
```

**只读**

容器实例内部被限制，只能读取不能写

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142229658.png)

 /容器目录:ro 镜像名        就能完成功能，此时容器自己只能读取不能写

ro = read only

此时如果宿主机写入内容，可以同步给容器内，容器可以读取到。

```
 docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录:ro      镜像名
```

#### 卷的继承和共享

容器1完成和宿主机的映射

```
docker run -it --privileged=true -v /mydocker/u:/tmp --name u1 ubuntu
```

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142230177.png)



![img](C:/Users/ljr/AppData/Roaming/Edraw/MindMaster/kingsoft/office6/151e313d470/bin/A3889BDD-487D-48CA-8A79-46AF68F9B588.png)

容器2继承容器1的卷规则

![img](https://gitee.com/liujunrull/image-blob/raw/master/202305142230730.png)

```
docker run -it  --privileged=true --volumes-from 父类  --name u2 ubuntu
```

## Docker常规安装简介

## Docker复杂安装详说

## DockerFile解析

## Docker微服务实战

## Docker网络

## Docker-compose容器编排

## Docker轻量级可视化工具Portainer

## Docker容器监控之CAdvisor+InfluxDB+Granfana

## 终章の总结

