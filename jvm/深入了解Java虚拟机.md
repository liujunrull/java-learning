##  走近Java

![](https://gitee.com/liujunrull/image-blob/raw/master/202211030950505.png)

- Java ME（Micro Edition） ： 支持Java程序运行在移动终端（手机、 PDA） 上的平台， 对Java API有所精简， 并加入了移动终端的针对性支持， 这条产品线在JDK 6以前被称为J2ME。 有一点读者请勿混淆， 现在在智能手机上非常流行的、 主要使用Java语言开发程序的Android并不属于Java ME。
- Java SE（Standard Edition） ： 支持面向桌面级应用（如Windows下的应用程序） 的Java平台， 提供了完整的Java核心API， 这条产品线在JDK 6以前被称为J2SE。
- Java EE（Enterprise Edition） ： 支持使用多层架构的企业应用（如ERP、 MIS、 CRM应用） 的Java平台， 除了提供Java SE API外， 还对其做了大量有针对性的扩充， 并提供了相关的部署支持，这条产品线在JDK 6以前被称为**J2EE**， 在JDK 10以后被Oracle放弃， 捐献给Eclipse基金会管理， 此后被
称为Jakarta EE。

### 运行时数据区域

![](https://gitee.com/liujunrull/image-blob/raw/master/202211070955060.png)

#### 程序计数器

程序计数器（Program Counter Register） 是一块较小的内存空间， 它可以看作是当前线程所执行的字节码的**行号指示器**。 在Java虚拟机的概念模型里， 字节码解释器工作时就是通过改变这个计数器的值来选取下一条需要执行的字节码指令， 它是程序控制流的指示器， 分支、 循环、 跳转、 异常处理、 线程恢复等基础功能都需要依赖这个计数器来完成。

由于Java虚拟机的多线程是通过线程轮流切换、 分配处理器执行时间的方式来实现的， 在任何一个确定的时刻， 一个处理器（对于多核处理器来说是一个内核） 都只会执行一条线程中的指令。 因此， 为了线程切换后能恢复到正确的执行位置， 每条线程都需要有一个**独立**的程序计数器， 各条线程之间计数器互不影响， 独立存储， 我们称这类内存区域为“线程私有”的内存。

如果线程正在执行的是一个Java方法， 这个计数器记录的是正在执行的虚拟机字节码指令的**地址**； 如果正在执行的是本地（Native） 方法， 这个计数器值则应为**空（Undefined）** 。 此内存区域是唯一一个在《Java虚拟机规范》 中没有规定任何OutOfMemoryError情况的区域。

#### Java虚拟机栈

与程序计数器一样， Java虚拟机栈（Java Virtual Machine Stack） 也是线程私有的， 它的生命周期与线程相同。 虚拟机栈描述的是Java方法执行的线程内存模型： 每个方法被执行的时候， Java虚拟机都会同步创建一个栈帧（Stack Frame） 用于存储局部变量表、 操作数栈、 动态连接、 方法出口等信息。 每一个方法被调用直至执行完毕的过程， 就对应着一个栈帧在虚拟机栈中从入栈到出栈的过程

局部变量表存放了编译期可知的各种Java虚拟机基本数据类型（boolean、byte、 char、 short、 int、float、 long、 double） 、 对象引用（reference类型， 它并不等同于对象本身， 可能是一个指向对象起始地址的引用指针， 也可能是指向一个代表对象的句柄或者其他与此对象相关的位置） 和returnAddress类型（指向了一条字节码指令的地址） 。

这些数据类型在局部变量表中的存储空间以局部变量槽（Slot） 来表示， 其中64位长度的**long和double**类型的数据会占用两个变量槽， 其余的数据类型只占用一个。 局部变量表所需的内存空间在编译期间完成分配， 当进入一个方法时， 这个方法需要在栈帧中分配多大的局部变量空间是完全确定的， 在方法运行期间不会改变局部变量表的大小。 请读者注意， 这里说的“大小”是指变量槽的数量，虚拟机真正使用多大的内存空间（譬如按照1个变量槽占用32个比特、 64个比特， 或者更多） 来实现一个变量槽， 这是完全由具体的虚拟机实现自行决定的事情。

在《Java虚拟机规范》 中， 对这个内存区域规定了两类异常状况： 如果线程请求的栈深度大于虚拟机所允许的深度， 将抛出StackOverflowError异常； 如果Java虚拟机栈容量可以动态扩展， 当栈扩展时无法申请到足够的内存会抛出OutOfMemoryError异常。

#### 本地方法栈

本地方法栈（Native Method Stacks） 与虚拟机栈所发挥的作用是非常相似的， 其区别只是虚拟机栈为虚拟机执行Java方法（也就是字节码） 服务， 而本地方法栈则是为虚拟机使用到的本地（Native）方法服务

#### Java堆

Java堆（Java Heap） 是虚拟机所管理的内存中最大的一块。 Java堆是被所
有线程共享的一块内存区域， 在虚拟机启动时创建。 此内存区域的唯一目的就是存放对象实例， Java世界里“几乎”所有的对象实例都在这里分配内存。 

#### 方法区

方法区（Method Area） 与Java堆一样， 是各个线程共享的内存区域， 它用于存储已被虚拟机加载的类型信息、 常量、 静态变量、 即时编译器编译后的代码缓存等数据

#### 运行时常量池

运行时常量池（Runtime Constant Pool） 是方法区的一部分。 Class文件中除了有类的版本、 字段、 方法、 接口等描述信息外， 还有一项信息是常量池表（Constant Pool Table） ， 用于存放编译期生成的各种字面量与符号引用， 这部分内容将在类加载后存放到方法区的运行时常量池中。

#### 直接内存

管理员配置虚拟机参数时， 会根据实际内存去设置-Xmx等参数信息， 但经常忽略掉直接内存， 使得各个内存区域总和大于物理内存限制（包括物理的和操作系统级的限制） ， 从而导致动态扩展时出现OutOfMemoryError异常。

## 虚拟机类加载机制

#### 概述

**虚拟机的类加载机制**

java虚拟机把描述类的数据从class文件加载到内存，并对数据进行校验、转换解析、初始化，最终形成可以被虚拟机直接使用的java类型。



类型的加载、连接和初始化过程都是在程序运行期间完成的，java天生可以动态扩展的语言特性就是依赖运行期动态加载和动态连接这个特点实现的。

#### 类加载的时机

一个类型从被加载到虚拟机内存开始到卸载出内存，整个生命周期将会经历**加载、验证、准备、解析初始化、使用、卸载**七个阶段，其中**验证、准备、解析**三部分称为连接

**必须对类进行初始化（连接需要在此之前进行）情况，称为对一个类型进行主动引用**

1. 遇到new、getstatic、putstatic、invokestatic这四条字节码指令时，如果类型之前没有进行过初始化
   - [ ] ​	使用new关键字
   - [ ] 读取或设置一个类型的静态字段（被final修饰、已在编译器将结果放入常量池的静态字段除外
   - [ ] 调用一个类型的静态方法的时候
2. 使用java.lang.reflect包的方法对类型进行反射调用的时候
3. 当初始化类时，发现父类还没有初始化，先初始化父类
4. 当虚拟机启动，用户需要指定一个要执行的主类（包含main()方法的那个类）
5. 当使用jdk7新加入的动态语言支持时，解析结果为REF_getstatic、REF——putStatic等
6. jdk8新加入的默认方法（被default关键字修饰的接口方法）

#### 类加载的过程

1. 通过一个类的全限定名来获取定义此类的二进制字节流
2. 将这个字节流所代表的静态存储结构转化为方法区的运行时数据结构
3. 在内存中生成一个代表这个类的java,lang.Class对象，作为方法区这个类的各种数据的访问入口