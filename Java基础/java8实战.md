## Lambda表达式

```java
哪些不是有效的Lambda表达式
(1)() -> {}
(2)() -> "aaa"
(3)() -> {return "aaa"}
(4)(Integer i) -> return "a" + i
(5)(String s) -> {"aaa"}
```

4.5无效。有retrun需要配合{}一起使用，无return不需要{}

**函数式接口是只定义一个抽象方法的接口**

Lambda表达式允许你直接以内联的形式为函数式接口的抽象方法提供实现，并把整个表达式作为函数式接口的实例（具体来说，是函数式接口一个具体实现的实例）

```java
public class Main {
    public static void main(String[] args) {
        Runnable r1 = () -> System.out.println("hello world1");
        Runnable r2 = new Runnable() {
            @Override
            public void run() {
                System.out.println("hellow world2");
            }
        };

      process(r1);
      process(r2);
      process(() -> System.out.println("hello world3"));
    }


    public static void process(Runnable r){
        r.run();
    }

}
//hello world1
//hellow world2
//hello world3
```

#### 使用局部变量

```java
int portNumber = 1339;
Runnable r = () -> System.out.println(portNumber);
portNumber = 32222;//错误，Lambda表达式引用的局部变量必须是最终的（final)或事实上是最终的
```

为什么会有这个限制？

实例变量和局部变量背后的实现有一个关键不同。实例变量都存储在**堆**中，而局部变量保存在**栈**上。如果Lambda可以直接访问局部变量，而且Lambda是在一个线程中使用的，则使用Lambda的线程，可能会在分配该变量的线程将这个变量收回之后，去访问该变量。因此，加吧在访问自由局部变量时，实际上是在访问它的副本，而不是访问原始变量。如果局部变量仅仅赋值一次那就没什么区别了——因此有了这个限制

#### 闭包

你可能已经听说过闭包( closure，不要和Clojure编程语言混淆）这个词，你可能会想Lambda是否满足闭包的定义。用科学的说法来说，闭包就是一个函数的实例，且它可以无限制地访问那个函数的非本地变量。例如，闭包可以作为参数传递给另一个函数。它也可以访问和修改其作用域之外的变量。现在，Java 8的Lambda和匿名类可以做类似于闭包的事情:它们可以作为参数传递给方法，并且可以访问其作用域之外的变量。但有一个限制:它们不能修改定义Lambda的方法的局部变量的内容。这些变量必须是隐式最终的。可以认为Lambda是对值封闭，而不是对变量封闭。如前所述，这种限制存在的原因在于局部变量保存在栈上,并且隐式表示它们仅限于其所在线程。如果允许捕获可改变的局部变量，就会引发造成线程不安全的新的可能性，而这是我们不想看到的(实例变量可以，因为它们保存在堆中，而堆是在线程之间共享的)。

#### 流只能消费一次

```java
List<String> title = Arrays.asList("Java8","In","Action");
Stream<String> s = title.stream();
s.foreach(System.out::println);
s.foreach(System.out::println);//流已被操作或关闭
```

- optional里面几种可以迫使你显式地检查值是否存在或处理值不存在的情形的方法也不错。
  isPresent()将在optional包含值的时候返回true,否则返回false。
  ifPresent(Consumer<T> block)会在值存在的时候执行给定的代码块。我们在第3章介绍了consumer函数式接口;它让你传递一个接收r类型参数，并返回void的Lambda表达式。
  T get ()会在值存在时返回值，否则抛出一个NosuchElement异常。

  T orElse(T other)会在值存在时返回值，否则返回一个默认值。
  例如,在前面的代码中你需要显式地检查optional对象中是否存在一道菜可以访问其名称:

  ```java
  map.stream.flter(Dish.isVegetarian).findAny().ifPresent(d -> System.out.println(d.getName));
  ```

- 何时使用findFirst和findAny
  你可能会想，为什么会同时有findFirst和findAny呢?答案是并行。找到第一个元素在并行上限制更多。如果你不关心返回的元素是哪个，请使用findAny，因为它在使用并行流时限制较少。

- reduce操作

```java
List<Integer> list = Arrays.asList(1, 2, 4, 4, 5, 6, 8, 9);
int sum = list.stream().reduce(0,(a,b) -> a + b);
int sum1 = list.stream().reduce(0,Integer:: sum);//Interger提供的求和静态方法sum
```

reduce接受两个参数:

一个初始值，这里是0;
一个Binary0perator<T>来将两个元素结合起来产生一个新值，这里我们用的是lambda (a, b) -> a + b。

- 将列表中的交易按货币分组

```java
Map<Currency,List<Transaction>> transaction = transactions.stream().collect(groupingBy(Transation::getCurrency));
```

#### CompletableFuture

```java
public class FutureTest {
    public Future<Double> getPriceAsunch(String product) {
        CompletableFuture<Double> futurePricreasunch = new CompletableFuture<>();
        new Thread(() -> {
            try{
                double price = cal(product);
                int i = 10/0;
                futurePricreasunch.complete(price);

            }catch(Exception e){
                futurePricreasunch.completeExceptionally(e);
            }

        }).start();
        return futurePricreasunch;
    }
    
    public Future<Double> getPriceAsync1(String product){
        return CompletableFuture.supplyAsync(() -> cal(product));
    }
    
    private double cal(String product) {
        return 10.01;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTest futureTest = new FutureTest();
        Future<Double> pinggio = futureTest.getPriceAsunch("pinggio");
        System.out.println(pinggio.get().toString());
    }
}


Exception in thread "main" java.util.concurrent.ExecutionException: java.lang.ArithmeticException: / by zero
	at java.util.concurrent.CompletableFuture.reportGet(CompletableFuture.java:357)
	at java.util.concurrent.CompletableFuture.get(CompletableFuture.java:1895)
	at test20230831.FutureTest.main(FutureTest.java:38)
Caused by: java.lang.ArithmeticException: / by zero
	at test20230831.FutureTest.lambda$getPriceAsunch$0(FutureTest.java:20)
	at java.lang.Thread.run(Thread.java:748)

```



